       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONTROL2.
       AUTHOR. PASIN-RWRK.
       
       ENVIRONMENT DIVISION. 
       CONFIGURATION SECTION. 
       SPECIAL-NAMES. 
           CLASS HEX-NUMBER IS "0" THRU "9", "A" THRU "F"
           CLASS REAL-NAME IS "A" THRU "Z", "a" THRU "z", "'", SPACE.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  NUM-IN   PIC X(4).
       01  NAME-IN  PIC X(15).
       
       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter a Hex number - " WITH NO ADVANCING 
           ACCEPT NUM-IN
           IF NUM-IN IS HEX-NUMBER THEN
              DISPLAY NUM-IN  " is a Hex Number"
           ELSE
              DISPLAY NUM-IN  " is NOT a Hex Number"
           END-IF 

           DISPLAY "Enter a Real NAME - " WITH NO ADVANCING 
           ACCEPT NAME-IN 
           IF NAME-IN  IS REAL-NAME THEN
              DISPLAY NAME-IN   " is a  RealName"
           ELSE
              DISPLAY NAME-IN   " is NOT a Realname"
           END-IF 
           
           DISPLAY "Enter a NAME - " WITH NO ADVANCING 
           ACCEPT NAME-IN 
           IF NAME-IN  IS ALPHABETIC  THEN
              DISPLAY NAME-IN   " is a  Name"
           ELSE
              DISPLAY NAME-IN   " is NOT a name"
           END-IF 
           .