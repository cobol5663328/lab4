       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONTROL4.
       AUTHOR. PASIN-RWRK.
       
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT STUDENT-FILE ASSIGN TO "GRADE.DAT"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  STUDENT-FILE.
       01  STUDENT_DETAILS.
           88 END-OF-STUDENT-FILE VALUE HIGH-VALUE .
           05 STUDENT-ID     PIC X(8).
           05 STUDENT-NAME   PIC X(16).
           05 COURSE-CODE    PIC X(8).
           05 GRADE          PIC X(2).
       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT STUDENT-FILE 
           PERFORM UNTIL END-OF-STUDENT-FILE 
              DISPLAY STUDENT-NAME SPACE STUDENT-ID SPACE COURSE-CODE 
                 SPACE GRADE  
              READ  STUDENT-FILE
              AT END SET END-OF-STUDENT-FILE TO TRUE 
              END-READ
           END-PERFORM
           CLOSE STUDENT-FILE
           . 