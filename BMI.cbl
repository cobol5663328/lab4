       IDENTIFICATION DIVISION.
       PROGRAM-ID. bmi.
       AUTHOR. PASIN-RWRK.

       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       01  BMI PIC 99.99 VALUE ZERO .
       01  H  PIC  999V99 VALUE ZERO .
       01  W  PIC  999 VALUE ZERO.
       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "ENTER A WEIGHT(KG.) :-" WITH NO ADVANCING 
              ACCEPT W 
           DISPLAY "ENTER A HEIGHT(M.) :-" WITH NO ADVANCING 
              ACCEPT H 
           
           COMPUTE BMI = ( W / ( H * H ) ).

           DISPLAY BMI
           .
